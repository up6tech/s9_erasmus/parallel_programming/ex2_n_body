cmake_minimum_required(VERSION 3.17)
project(ex2_nbody_simulation)

set(CMAKE_CXX_STANDARD 14)

find_package(MPI REQUIRED)
message(STATUS "Run: ${MPIEXEC} ${MPIEXEC_NUMPROC_FLAG} ${MPIEXEC_MAX_NUMPROCS} ${MPIEXEC_PREFLAGS} EXECUTABLE ${MPIEXEC_POSTFLAGS} ARGS")


add_executable(ex2_nbody_simulation main.cpp)
target_link_libraries(${PROJECT_NAME} PUBLIC MPI::MPI_CXX)

#find_package(OpenMP)
#if(OpenMP_CXX_FOUND)
#    target_link_libraries(${PROJECT_NAME} PUBLIC OpenMP::OpenMP_CXX)
#endif()


/**
 * @file main.cpp
 * @author Fabien CAYRE (fabien.cayre@abo.fi)
 * @author Fatou TAL (fatou.tall@abo.fi)
 * @brief Compute N-body simulation across multiple cores
 * @version 0.2
 * @date 09/10/2022
 *
 * @documents Documents used for this program
 *
 * @docbookinclude
 * https://moodle.abo.fi/pluginfile.php/953014/mod_resource/content/4/Messagepassing_4.pdf
 * @docbookinclude
 * https://stackoverflow.com/questions/33618937/trouble-understanding-mpi-type-create-struct
 * @docbookinclude
 * https://www.mpich.org/static/docs/v3.1/www3/MPI_Type_create_struct.html
 * @docbookinclude
 * https://rookiehpc.github.io/mpi/docs/mpi_type_create_struct/index.html
 * ^^^ Documentations about the creation of custom type in MPI
 *
 * @docbookinclude
 * https://moodle.abo.fi/pluginfile.php/953008/mod_resource/content/3/Messagepassing_4.pdf
 *
 * ^^^ Collective communication
 *
 * @test
 *
 * mpirun -n 1 ex2_nbody_simulation produce the same result as "Nbody.c" program
 * mpirun -n 2 ex2_nbody_simulation produce weird result
 *
 * @copyright Copyright (c) 2022
 *
 */

#include <iostream>
#include <math.h>
#include <stdlib.h>

const double G = 6.67259e-7; /* Gravitational constant (should be e-10 but
                                modified to get more action */
const double dt = 1.0;       // Timestep length
const int N = 100;           // Number of bodies
const int timesteps = 1000;  // Number of timesteps
const double size = 100.0;   // Initial positions are in the range [0,100]
const double mindist =
    0.0001; /* Minimal distance of two bodies of being in interaction*/

/**
 * @brief represent a body
 *
 */
struct body {
  //
  int process_id;
  // x position
  double x;
  // y position
  double y;
  // old x position
  double old_x;
  // old y position
  double old_y;
  // mass
  double mass;
  // x force
  double fx;
  // y force
  double fy;
  // x velocity
  double vx;
  // y velocity
  double vy;
};

typedef struct body body;

void free_bodies(body *bodies, int count) { free(bodies); }

void debug(int set1, int set2, body bodyS1, body bodyS2) {
  // if (bodyS1.process_id == 0 && bodyS2.process_id != 0) {
  //   std::cout << "Update body " << set1 << ":P" << bodyS1.process_id
  //             << " With data of body " << set2 << ":P" << bodyS2.process_id
  //             << std::endl;
  //   printf("Body1 [x=%f,y=%f,vx=%f,vy=%f,mass=%f]\n", bodyS1.x, bodyS1.y,
  //          bodyS1.vx, bodyS1.vy, bodyS1.mass);
  //   printf("Body2 [x=%f,y=%f,vx=%f,vy=%f,mass=%f]\n", bodyS2.x, bodyS2.y,
  //          bodyS2.vx, bodyS2.vy, bodyS2.mass);
  // }
}

void debug_body(body b, int process_id) {
  printf("[P%d] Body [x=%f,y=%f,vx=%f,vy=%f,mass=%f,pid=%d]\n", process_id, b.x,
         b.y, b.vx, b.vy, b.mass, b.process_id);
}
void debug_bodies(body *bodies, int count, int process_id) {
  for (size_t i = 0; i < count; i++) {
    debug_body(bodies[i], process_id);
  }
}
/**
 * @brief Distance between two point P(x,y) and Q(x,y)
 *
 * @param px
 * @param py
 * @param qx
 * @param qy
 * @return double
 */
double dist(double px, double py, double qx, double qy) {
  return sqrt(pow(px - qx, 2) + pow(py - qy, 2));
}

int write_particles(int N, body *bodies, const std::string f_name,
                    int process_id, bool use_old_xy) {
  FILE *fp;
  /* Open the file */
  std::string file_name = f_name + "P" + std::to_string(process_id) + ".txt";
  auto c_file_name = file_name.c_str();

  if ((fp = fopen(c_file_name, "w")) == NULL) {
    printf("Couldn't open file %s\n", c_file_name);
    return 0;
  }
  /* Write the positions to the file fn */
  for (int i = 0; i < N; i++) {
    if (use_old_xy) {
      fprintf(fp, "%3.8f %3.8f \n", bodies[i].old_x, bodies[i].old_y);
    } else {
      fprintf(fp, "%3.8f %3.8f \n", bodies[i].x, bodies[i].y);
    }
  }
  fprintf(fp, "\n");
  fclose(fp); /* Close the file */
  return 1;
}

void init_force_vectors(int count, body *bodies) {
  for (int i = 0; i < count; i++) {
    auto body = bodies[i];
    body.fx = 0;
    body.fy = 0;
    bodies[i] = body;
  }
}

void compute_forces(int start, int end, body *bodies_set1, int start1, int end1,
                    body *bodies_set2) {
  for (int set1 = start; set1 < end; set1++) {
    auto bodyS1 = bodies_set1[set1];
    for (int set2 = start1; set2 < end1; set2++) {
      auto bodyS2 = bodies_set2[set2];
      // debug_body(bodyS2);
      if (bodyS2.process_id == bodyS1.process_id && set1 == set2)
        continue; // check if not the same bodies
      double r = dist(bodyS1.old_x, bodyS1.old_y, bodyS2.old_x, bodyS2.old_y);
      if (r > mindist) {
        double r3 = pow(r, 3);
        bodyS1.fx +=
            G * bodyS1.mass * bodyS2.mass * (bodyS2.old_x - bodyS1.old_x) / r3;
        bodyS1.fy +=
            G * bodyS1.mass * bodyS2.mass * (bodyS2.old_y - bodyS1.old_y) / r3;
      }
    }
    bodies_set1[set1] = bodyS1;
  }
}

/**
 * @brief
 *
 * @param argc
 * @param argv
 * @return int
 */
int main(int argc, char *argv[]) {
  int local_bodies_count = N;
  int process_id = 0;

  auto local_bodies =
      (struct body *)calloc(local_bodies_count, sizeof(struct body));

  double tt = 0;
  for (int i = 0; i < process_id * local_bodies_count; i++) {
    // pregen values to have the same initial position as "Nbody.c" program
    tt = 1000.0 * drand48();
    tt = size * drand48();
    tt = size * drand48();
  }
  std::cout << tt << std::endl;

  for (int i = 0; i < local_bodies_count; i++) {
    struct body body;
    body.mass = 1000.0 * drand48();
    body.old_x = size * drand48();
    body.old_y = size * drand48();
    body.process_id = process_id;
    local_bodies[i] = body;
  }

  write_particles(local_bodies_count, local_bodies, "initial_pos", process_id,
                  true);

  // Save position of one body so we can see where it has moved
  double pos0x = local_bodies[0].old_x;
  double pos0y = local_bodies[0].old_y;

  // Compute the initial forces that we get
  init_force_vectors(local_bodies_count, local_bodies);
  compute_forces(local_bodies_count, local_bodies, // SET 1
                 local_bodies_count, local_bodies, // SET 2
                 process_id                        // process_id
  );

  // Set up the velocity vectors caused by initial forces for Leapfrog method
  for (int i = 0; i < local_bodies_count; i++) {
    auto bodyI = local_bodies[i];

    bodyI.vx = 0.5 * dt * bodyI.fx / bodyI.mass;
    bodyI.vy = 0.5 * dt * bodyI.fy / bodyI.mass;

    local_bodies[i] = bodyI;
  }

  int t = 0;
  MPI_Barrier(MPI_COMM_WORLD);
  while (t < timesteps) { // Loop for this many timesteps
                          // while (t < 1) { // Loop for this many timesteps
    t++;
    // printf("%d ", t);
    // fflush(stdout); // Print out the timestep

    // Calculate new positions
    for (int i = 0; i < local_bodies_count; i++) {
      auto bodyI = local_bodies[i];

      bodyI.x = bodyI.old_x + bodyI.vx * dt;
      bodyI.y = bodyI.old_y + bodyI.vy * dt;

      local_bodies[i] = bodyI;
    }

    /* Calculate forces for the new positions */
    /* Compute self */
    init_force_vectors(local_bodies_count, local_bodies);
    compute_forces(local_bodies_count, local_bodies, // SET 1
                   local_bodies_count, local_bodies, // SET 2
                   process_id                        // process_id
    );

    for (int other_process = 0; other_process < process_count;
         other_process++) {
      if (other_process == process_id)
        continue;

      MPI_Send(local_bodies, local_bodies_count, mpi_body_type, other_process,
               0, MPI_COMM_WORLD);
      MPI_Recv(foreign_bodies, local_bodies_count, mpi_body_type, other_process,
               0, MPI_COMM_WORLD, MPI_STATUSES_IGNORE);
      // receive foreign changes from other_process
      compute_forces(local_bodies_count, local_bodies, local_bodies_count,
                     foreign_bodies, process_id);
    }

    /* Update velocities of bodies */
    for (int i = 0; i < local_bodies_count; i++) {
      auto bodyI = local_bodies[i];

      bodyI.vx = bodyI.vx + bodyI.fx * dt / bodyI.mass;
      bodyI.vy = bodyI.vy + bodyI.fy * dt / bodyI.mass;

      local_bodies[i] = bodyI;
    }

    /* Copy updated positions to (X_old, Y_old) before next time step begins */
    for (int i = 0; i < local_bodies_count; i++) {
      auto bodyI = local_bodies[i];
      bodyI.old_x = bodyI.x;
      bodyI.old_y = bodyI.y;

      local_bodies[i] = bodyI;
    }

  } /* end of while-loop */

  printf("\n");
  if (process_id == 0) {
    std::cout << __LINE__ << std::endl;
  }

  // int res = write_particles(local_bodies_count, local_bodies,
  // "final_pos.txt",
  //                           true);
  // if (res < 1) {
  //   std::cerr << "Error write particle" << std::endl;
  // }

  // DEBUG
  double pos0_finalx = local_bodies[0].old_x;
  double pos0_finaly = local_bodies[0].old_y;

  printf("[P%d] Original pos %3.8f %3.8f\n", process_id, pos0x, pos0y);
  printf("[P%d] Final pos    %3.8f %3.8f\n", process_id, pos0_finalx,
         pos0_finaly);
  printf("[P%d] count: %d  foreign ptr: %p, local ptr: %p\n", process_id,
         local_bodies_count, foreign_bodies, local_bodies);

  write_particles(local_bodies_count, local_bodies, "final_pos", process_id,
                  false);

  free_bodies(foreign_bodies, local_bodies_count);
  free_bodies(local_bodies, local_bodies_count);

  double end = MPI_Wtime();

  std::cout << "Code executed in " << ((end - start) * 1000) << "ms"
            << std::endl;

  MPI_Finalize();

  return 0;
}

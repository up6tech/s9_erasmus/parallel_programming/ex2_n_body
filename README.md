# N-body simulation spread over multiple process

Using a custom MPI struct `body` with 10 fields, one for the process id (`INT`) and 9 others (`DOUBLE`) for body attributes (mass, positions, acceleration, velocities..).

After a few tests on my laptot, i realized that this struct was too big for my computer.
So was with fatou's computer, so we decided to create another struct, called `body_fast` that represent only the necessary passing information between processes.

# On a laptop

Model : Intel(R) Core(TM) i7-4800MQ CPU @ 2.70GHz
Total cores: 4

```shell
mpirun -n 1 ex2_nbody_simulation
```

Typical output:

```
N-body simulation (Process n°0)

357
[P0] Original pos 33.45458315 40.26470512
[P0] Final pos    353.58785170 85.91945228
[P0] count: 100  foreign ptr: 0x22d4ed0, local ptr: 0x22d6e20
Code executed in 1150.63ms
```

2 files are generated: "initial_P\<pid>.txt" and "final_P\<pid>.txt".

**On average, the code on 1 process execute in 1149.59ms** _(Mean of 5 timed execution)_

# Table of results

Time are shown in milliseconds.
Each time is the mean of 5 measurements.

## Laptop

| X              | Processors |           |           |
| -------------- | ---------- | --------- | --------- |
| **Body count** | 1          | 2         | 4         |
| **100**        | 1149.59    | 528.8     | 367.4724  |
| **200**        | 4067.98    | 2102.46   | 1342.28   |
| **400**        | 16397.2    | **Dying** | **Dying** |

As you can see, our computer could not handle the 400 bodies transfert with multi process.

## Dione

| X          | Processors |           |           |           |           |
| ---------- | ---------- | --------- | --------- | --------- | --------- |
| **Bodies** | 1          | 2         | 4         | 8         | 16        |
| **100**    | 1044.43    | 705.7     | 457.33    | 220.4     | 146.8     |
| **200**    | 4186.98    | 2815.46   | 1776.28   | 862.3     | 428.6     |
| **400**    | 16808      | **Dying** | **Dying** | **Dying** | **Dying** |

On Dione too, when it comes to handle / transmit 400 bodies accross multiple processes, it crash.
